# brc-StyleSwapMod



A mod for Bomb Rush Cyberfunk that allows you to swap movestyles mid combo.


## Controls


Hold your ride movestyle button and press trick 1 for inline, trick 2 for skateboard or trick 3 for BMX.


## Example


https://youtu.be/aNSiu_6UTbQ
