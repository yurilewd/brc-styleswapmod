﻿using BepInEx;
using HarmonyLib;
using Reptile;

namespace brc_styleswapmod
{
    [BepInPlugin(MyGUID, PluginName, VersionString)]
    public class brc_styleswapmodPlugin : BaseUnityPlugin
    {
        
        private const string MyGUID = "com.yuril.brc_styleswapmod";
        private const string PluginName = "brc_styleswapmod";
        private const string VersionString = "1.0.1";

        private Core core;
        private WorldHandler world;
        private bool coreHasBeenSetup;
        private bool delegateHasBeenSetup = false;
        private Player player;
        private UserInputHandler.InputBuffer input;

        private void Awake()
        {
            Logger.LogInfo($"Finished Loading!");
        }

        private void Update()
        {
            if (!coreHasBeenSetup)
            {
                core = Core.Instance;
                if (core != null)
                {
                    world = WorldHandler.instance;
                    coreHasBeenSetup = world != null;

                    if (!delegateHasBeenSetup)
                    {
                        StageManager.OnStageInitialized += () =>
                        {
                            coreHasBeenSetup = false;
                        };
                        delegateHasBeenSetup = true;
                    }
                }
            }
            if (coreHasBeenSetup)
            {
                if (player == null)
                {
                    player = world.GetCurrentPlayer();
                }
                else
                {
                    input = (UserInputHandler.InputBuffer)Traverse.Create(player).Field("inputBuffer").GetValue();

                    if (input.switchStyleButtonHeld)
                    {
                        if (input.trick1ButtonNew)
                        {
                            SwapStyle(MoveStyle.INLINE);
                        }

                        else if (input.trick2ButtonNew)
                        {
                            SwapStyle(MoveStyle.SKATEBOARD);
                        }

                        else if (input.trick3ButtonNew)
                        {
                            SwapStyle(MoveStyle.BMX);
                        }
                    }
                }
            }
        }    
               

        private void SwapStyle(MoveStyle NewStyle)
        {

            Ability currentAbility = (Ability)Traverse.Create(player).Field("ability").GetValue();
            GrindAbility playerGrindAbility = (GrindAbility)Traverse.Create(player).Field("grindAbility").GetValue();
            WallrunLineAbility playerWallrunAbility = (WallrunLineAbility)Traverse.Create(player).Field("wallrunAbility").GetValue();

            player.InitMovement(NewStyle);

            if (currentAbility == playerGrindAbility)
            {
                player.SwitchToEquippedMovestyle(true, false, true, true);
            }

            else if (currentAbility == playerWallrunAbility)
            {
                player.SwitchToEquippedMovestyle(true, true, true, true);
            }

            else
            {
                if ((bool)Traverse.Create(player).Method("CanJump").GetValue())
                {
                    player.Jump();
                }
                player.SwitchToEquippedMovestyle(true, true, true, true);
            }
        }
    }
}

